'use strict'

import React from 'react';
import ReactDOM from 'react-dom';

export class App extends React.Component {
	render() {
		return (
			<div className="app">React App Component</div>
		);
	}
}

ReactDOM.render(<App/>, document.getElementById('react-app'));