### React-JS-Boilerplate ###

Client side reactjs boilerplate with gulp, browserify, sass, mocha, and bootstrap to get started writing es6 right away.

### Set up ###

clone repo - ``` git clone git@bitbucket.org:edfialk/react-js-boilerplate.git ```

### Structure ###
```
app
|- build
|- client
   |- sass
|- public
   |- scripts
   |- styles
|- server
|- test
```

### Use ###

``` gulp test ```

* [jshint](https://github.com/jshint/jshint)
* browserify client/test javascript
* run mocha tests

``` gulp build ```

* browserify /client/index.js
* uglify /build/app.js
* sass /client/sass
* minify /build/app.css
* copy to /public/

``` gulp watch ```

* sass /client/sass/*.scss
* browserify /client/**/*.js
* test /test/client/*.js

### Contact ###

edfialk@gmail.com
http://edfialk.com