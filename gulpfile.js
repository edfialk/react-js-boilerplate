// Gulp Dependencies
var gulp = require('gulp');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var buffer = require('vinyl-buffer');

// Build Dependencies
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var babelify = require('babelify');
var babel = require('gulp-babel');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');

// Style Dependencies
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');


// Test Dependencies
var mochaPhantomjs = require('gulp-mocha-phantomjs');

gulp.task('javascript', function () {
  browserify({
    entries: 'client/index.js',
    extensions: ['.jsx', '.js'],
    debug: true
  })
  .transform(babelify)
  .bundle()
  .pipe(source('app.js'))
  .pipe(gulp.dest('public/scripts'));
});

gulp.task('test', function() {
  return gulp.src('test/client/index.html')
    .pipe(mochaPhantomjs());
});

gulp.task('styles', function() {
  return gulp.src('client/sass/app.scss')
    .pipe(sass())
    .pipe(prefix({ cascade: true }))
    .pipe(rename('app.css'))
    .pipe(gulp.dest('build'))
    .pipe(gulp.dest('public/styles'));
});

gulp.task('minify', ['styles'], function() {
  return gulp.src('build/app.css')
    .pipe(minifyCSS())
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest('public/styles'));
});

gulp.task('uglify', ['javascript'], function() {
  return gulp.src('build/app.js')
    .pipe(uglify())
    .pipe(rename('app.min.js'))
    .pipe(gulp.dest('public/scripts'));
});

gulp.task('build', ['uglify', 'minify']);

gulp.task('watch', function() {
  gulp.watch('client/sass/*.scss', ['styles']);
  gulp.watch('client/index.js', ['javascript', 'test']);
  gulp.watch('client/**/*.js', ['javascript', 'test']);
  gulp.watch('test/client/**/*.js', ['test']);
});

gulp.task('default', ['test', 'build', 'watch']);